INSTALL = install

DESTDIR =

sysconfdir = /etc

all:

install:
	$(INSTALL) -d $(DESTDIR)$(sysconfdir)/falco/rules.d/
	for r in rules/*.yml; do if grep -q -E 'tags: .*nonworking.*' "$$r"; then continue; fi; echo "# $$r" ; cat "$$r"; echo ; done > \
	    $(DESTDIR)$(sysconfdir)/falco/rules.d/nidhogg.yml
