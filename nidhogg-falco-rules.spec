Summary: Nidhogg Falco rules collection
Name: nidhogg-falco-rules
Version: 0.0.0
Release: 1
Copyright: GPL
Group: System Environment/Daemons
Source: nidhogg-falco-rules-%{version}.tar.gz
Requires: make
BuildRoot: /var/tmp/syslog-root

%description
Nidhogg Falco rules collection is a set of Falco rules to try to
detect Linux related security issues not already being detected by the
rules included with the Falco project itself.


%prep
%setup -q -n nidhogg-falco-rules-%{version}

%build
make %{?_smp_mflags}

%install
make install DESTDIR=$RPM_BUILD_ROOT

%clean
true

%files
%defattr(-,root,root)
%doc README*
%config /etc/falco/rules.d/nidhogg.yml

%changelog
* Mon Sep  5 2021 Petter Reinholdtsen <pere@hungry.com> 0.0.0-1
- Initial build.
