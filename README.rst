Nidhugg Falco rule collection
=============================

Nidhugg (Níðhöggr, i.e. Malice Striker) is the Norse mythology serpent
gnawing on and trying to kill the life tree Yggdrasil.

Nidhogg Falco rules collection is a set of `Falco <https://falco.org>`_
rules to try to detect Linux related security issues not already being
detected by the rules included with the Falco project itself.

The rule syntax is documented in
`the Falco rule documentation <https://falco.org/docs/rules/>`_.

The latest version of these rules can be fetched from the `Codeberg project
page <https://codeberg.org/pere/nidhogg-falco-rules>`_.

Get in touch with the authors using the issue tracker there.

Useful sources for security related issues:

* https://github.com/elastic/detection-rules/tree/main/rules
* https://github.com/SigmaHQ/sigma/tree/master/rules/linux
* https://attack.mitre.org/matrices/enterprise/linux/
